<?php
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use \yii\helpers\Url;
use kartik\daterange\DateRangePicker;
use common\models\Area;
use common\models\Exhibition;
use yii\widgets\Pjax;
use common\models\shop\Currency;

/* @var $searchModel backend\models\search\CurrencySearch */
?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><?=Yii::t('backend','Filter')?></h3>
    </div>

    <?php Pjax::begin(['clientOptions' => ['container' => '#currency-content']]); ?>
    <?php $form = ActiveForm::begin([
        'id' => 'currency-filter',
        'action' => ['index'],
        'method' => 'get',
        'options' => ['data-pjax' => true]
    ]); ?>
    <div class="box-body" style="">
        <div class="col-md-4">
            <?= $form->field($searchModel, 'currency')->dropDownList(['' => '--none--'] + Currency::valueCurrency())->label(false); ?>
        </div>
        <div class="form-group pull-right">
            <?= Html::submitButton(Yii::t('backend','Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend','Reset'), Url::toRoute('index'), ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
