<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $this yii\web\View
 * @var $model \common\models\shop\Currency
 * @var $dataProvider ActiveDataProvider
 * @var $rates \common\models\shop\Currency[]
 */

use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use common\models\shop\Currency;

$this->title = Yii::t('shop', 'Currency');
?>

<div class="ibox col-lg-6 currency-container">
    <div class="ibox-title">
        <div class="ibox-tools">
            <h5>
                <i class="glyphicon glyphicon-list">
                </i>
                <?= Yii::t('frontend', 'Add new currency') ?>
            </h5>
        </div>
        <div class="row">
            <div class="ibox-content">
                <div class="row">
                    <?php Pjax::begin(['clientOptions' => ['container' => '#currency-content']]); ?>
                    <table id="currency-table" class="table table-hover">
                        <?php $form = ActiveForm::begin([
                            'method' => 'post',
                            'options' => ['data' => ['pjax' => true]]
                        ]); ?>
                        <tr>
                            <th class="col-md-5"><?= Yii::t('shop', 'Rate'); ?></th>
                            <th class="col-md-5"><?= Yii::t('shop', 'Currency'); ?></th>
                            <th class="col-md-2"></th>
                        </tr>
                        <tr>
                            <th>
                                <?= $form->field($model, 'value')->textInput()->label(false); ?>
                            </th>
                            <th>
                                <?= $form->field($model, 'currency')->dropDownList(['' => '--none--'] + Currency::valueCurrency())->label(false); ?>
                            </th>
                            <th>
                                <?= Html::submitButton(Yii::t('shop', 'Add'), ['class' => 'btn btn-primary text-center']) ?>
                            </th>
                        </tr>
                        <?php ActiveForm::end(); ?>
                    </table>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ibox col-lg-6 currency-container">
    <div class="ibox-title">
        <div class="ibox-tools">
            <h5>
                <i class="glyphicon glyphicon-list">
                </i>
                <?= Yii::t('frontend', 'Currency') ?>
            </h5>
        </div>
        <?php Pjax::begin(['id' => 'currency-content']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => false,
            'options' => [
                'class' => 'project-list'
            ],
            'tableOptions' => [
                'id' => 'my-grid',
                'class' => 'table table-hover'
            ],
            'columns' => [
                [
                    'attribute' => 'value',
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:35%;'],
                ],
                [
                    'attribute' => 'currency',
                    'filter' => Currency::valueCurrency(),
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center'],
                    'value' => function ($model) {
                        return Currency::nameCurrency($model->currency);
                    }
                ],
                [
                    'attribute' => 'date',
                    'filter' => false,
                    'contentOptions' => ['class' => 'text-center'],
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:150px;'],
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDate($model->date);
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {remove}',
                    'headerOptions' => ['class' => 'text-center', 'style' => 'width:110px;'],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('',
                                Url::to(['update', 'id' => $model->id]),
                                ['class' => 'glyphicon glyphicon-edit btn btn-success', 'data-pjax' => 0]);
                        },
                        'remove' => function ($url, $model) {
                            return Html::a('',
                                Url::to(['remove', 'id' => $model->id]),
                                ['class' => 'glyphicon glyphicon-remove btn btn-danger', 'data-pjax' => 0]);
                        }
                    ],
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>