<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\rating\StarRating;
use yii\helpers\ArrayHelper;
use \common\models\shop\Product;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin([
        'id' => 'comment-form',
        'options' => [
            'name' => 'comment_work',
            'validateOnBlur' => false,
            'validateOnChange' => false,
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?php
    //    $form->field($model, 'rating')->widget(StarRating::classname(), [
    //        'pluginOptions' => [
    //            'size' => 'xs',
    //            'showClear' => false,
    //            'showCaption' => false,
    //            'theme' => 'krajee-svg',
    //            'filledStar' => '<span class="krajee-icon krajee-icon-star"></span>',
    //            'emptyStar' => '<span class="krajee-icon krajee-icon-star"></span>'
    //        ]
    //    ]);
    ?>


    <!--    <input id="input-id" name="input-name" type="number" class="rating" min=1 max=10 step=2 data-size="lg"-->
    <!--           data-rtl="true">-->

    <?= $form->field($model, 'rating')->textInput([
        "value" => $model->rating,
        "readonly" => (!$model->isNewRecord)
    ]) ?>

    <?= $form->field($model, 'approved')->checkbox() ?>

    <?php if ($model->isNewRecord) { ?>
        <?= $form->field($model, 'id_product')->textInput([
            "value" => $model->product->id,
            "readonly" => true
        ])->dropdownlist(ArrayHelper::map(Product::find()->all(), 'id', 'translation.title')); ?>
    <?php } else { ?>
        <?= $form->field($model, 'id_product')->textInput([
            "value" => $model->product->id,
            "readonly" => true
        ]) ?>

        <div class="form-group">
            <p><?= $model->product->translation->title ?></p>
        </div>
    <?php } ?>

    <?= $form->field($model, 'id_admin')->textInput([
        "value" => Yii::$app->user->id,
        "type" => "hidden",
        "readonly" => true
    ])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend.shop', "Add comment") : Yii::t('backend.shop', "Update comment"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
