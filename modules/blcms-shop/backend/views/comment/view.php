<?php

use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */

$this->title = Yii::t('backend.shop', "Comment");
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.shop', "Comments"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id_comment], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id_comment], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend.shop', "Are you shure?"),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_comment',
            'name',
            'comment:text',
            [
                'attribute' => 'created_date',
                'format' => 'datetime', // Возможные варианты: raw, html
                'content' => function ($data) {
                    return date("d.m.Y H:i", $data->created_date);
                }
            ],
            [
                'attribute' => 'modified_date',
                'format' => 'datetime', // Возможные варианты: raw, html
                'content' => function ($data) {
                    return date("d.m.Y H:i", $data->modified_date);
                }
            ],
            [
                'attribute' => 'approved',
                'format' => 'text',
                'content' => function ($data) {
                    return CheckboxX::widget([
                        'name' => 'status_approved' . $data->id_comment,
                        'value' => $data->approved,
                        'initInputType' => CheckboxX::INPUT_CHECKBOX,
                        'options' => [
                            'id' => 'status_approved' . $data->id_comment,
                            'readonly' => true
                        ],
                        'pluginOptions' => [
                            'threeState' => false,
                            'size' => 'sm',
                            'theme' => 'krajee-flatblue',
                            'enclosedLabel' => true,
                        ]
                    ]);
                },
            ],
            'id_product',
            'id_comment_ref',
            'id_admin',
            'id_user',
            'rating',
        ],
    ]) ?>

</div>
