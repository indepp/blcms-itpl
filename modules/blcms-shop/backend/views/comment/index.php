<?php

use kartik\checkbox\CheckboxX;
use webvimark\extensions\DateRangePicker\DateRangePicker;
use webvimark\extensions\GridBulkActions\GridBulkActions;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend.shop', "Comments");
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="comment-index">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-user-plus']) .
            Yii::t('shop', 'Create comment'), ['create'], ['class' => 'btn btn-primary btn-xs pull-right']);
        ?>

        <?php Pjax::begin(
            [
                'id' => 'comments-grid-pjax',
                'options' => [
                    'data-pjax-container' => 'comments-grid-pjax'
                ]
            ]
        ); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'pager' => [
                'options' => ['class' => 'pagination pagination-sm'],
                'hideOnSinglePage' => true,
                'lastPageLabel' => '>>',
                'firstPageLabel' => '<<',
            ],

            'layout' => '{items}
                        <div class="row">
                            <div class="col-sm-8">{pager}</div>
                            <div class="col-sm-4 text-right">
                                {summary}' . GridBulkActions::widget([
                    'gridId' => 'comments-grid-pjax',
                    'pjaxId' => 'comments-grid-pjax',
                    'actions' => [
                        Url::to(['bulk-activate', 'attribute' => 'approved']) =>
                            GridBulkActions::t('app', 'Activate'),
                        Url::to(['bulk-deactivate', 'attribute' => 'approved']) =>
                            GridBulkActions::t('app', 'Deactivate'),
                        '----' => [
                            Url::to(['bulk-delete']) =>
                                GridBulkActions::t('app', 'Delete'),
                        ],
                    ],
                ]) . '
                            </div>
                        </div>',

            'filterModel' => $searchModel,
            'columns' => [
//                ['class' => 'yii\grid\SerialColumn'],

                'id_comment',
                'name',
                [
                    'attribute' => 'approved',
                    'format' => 'html',
                    'content' => function ($data) {
                        return CheckboxX::widget([
                            'name' => 'status_approved' . $data->id_comment,
                            'value' => $data->approved,
                            'initInputType' => CheckboxX::INPUT_CHECKBOX,
                            'options' => [
                                'id' => 'status_approved' . $data->id_comment,
                                'readonly' => true
                            ],
                            'pluginOptions' => [
                                'threeState' => false,
                                'size' => 'sm',
                                'theme' => 'krajee-flatblue',
                                'enclosedLabel' => true,
                            ]
                        ]);
                    },
                    'filter' => array("1" => "Активно", "0" => "Не активно"),
                ],
                // 'rating',
                [
                    'attribute' => 'created_date',
                    'format' => 'datetime', // Возможные варианты: raw, html
                    'content' => function ($data) {
                        return date("d.m.Y H:i", $data->created_date);
                    }
                ],
                [
                    'attribute' => 'modified_date',
                    'format' => 'datetime', // Возможные варианты: raw, html
                    'content' => function ($data) {
                        return date("d.m.Y H:i", $data->modified_date);
                    }
                ],
                [
                    'label' => Yii::t('backend.shop', "Product id"),
                    'attribute' => 'id_product',
                    'format' => 'html', // Возможные варианты: raw, html
                    'content' => function ($data) {
                        return $data->product->id;
                    }
                ],
                [
                    'label' => Yii::t('backend.shop', "Product title"),
                    'format' => 'raw',
                    'value' => function ($data) {
                        return $data->product->translation->title;
                    }
                ],
//                [
//                    'format' => 'raw',
//                    'value' => function ($data) {
//                        return Html::a(
//                            'Ответы',
//                            Url::to(['/shop/comment/answers', 'id' => $data->id_comment]), [
//                            'target' => '_blank',
//                            'data' => [
//                                'pjax' => false
//                            ]
//                        ]);
//                    },
//                ],
                // 'id_comment_ref',
                // 'id_admin',

                ['class' => 'yii\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'headerOptions' => ['style' => 'color:#337ab7'],
                    'template' => '{view}{update}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'lead-view'),
                            ]);
                        },

                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'lead-update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'lead-delete'),
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => "Вы уверены?",
                                    'pjax' => false
                                ]
                            ]);
                        }

                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = Url::to(['/shop/comment/view', 'id' => $model->id_comment]);
                            return $url;
                        }

                        if ($action === 'update') {
                            $url = Url::to(['/shop/comment/update', 'id' => $model->id_comment]);
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url = Url::to(['/shop/comment/delete', 'id' => $model->id_comment, 'from' => 'index']);
                            return $url;
                        }

                    },
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?></div>

<?php
DateRangePicker::widget([
    'model' => $searchModel,
    'attribute' => 'created_date',
    'pluginOptions' => [
        'format' => 'DD.MM.YYYY'
    ]
]);

DateRangePicker::widget([
    'model' => $searchModel,
    'attribute' => 'modified_date',
    'pluginOptions' => [
        'format' => 'DD.MM.YYYY'
    ]
]) ?>