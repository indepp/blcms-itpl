<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */

$this->title = Yii::t('backend.shop', "Update comment: {name}", ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend.shop', "Comments"), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id_comment]];
$this->params['breadcrumbs'][] = Yii::t('backend.shop', "Update comment");
?>
<div class="comment-update">

    <?= $this->render('_form', [
        'model' => $model,
        'params' => $params
    ]) ?>

</div>
