<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $productId integer
 * @var $image_form ProductImageForm
 * @var $selectedLanguage \bl\multilang\entities\Language
 * @var $this \yii\web\View;
 */

use backend\modules\shop\components\form\ProductImageForm;
use common\models\shop\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use backend\widgets\filekit\Upload;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$js = <<<JS
$(document).on( "sortstop", "ul.files", function( event, ui ) {
    var list = ui.item.siblings();
    list.push(ui.item);
    var data = [];
    list.each(function() {
        var position = $(this).find('input[data-role="order"]').val();
        var id = $(this).find('input[data-role="id"]').val();
        data.push({'position':position,'id':id});
    });
    $.ajax({
        url: 'image-sort',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data)
    });
});
$(document).on('click', 'i.update-alt', function () {
    var url = $(this).data('url');
    var alt = $(this).siblings('input.alt').val();
    $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {'alt':alt}
    });
})
JS;
$this->registerJs($js,View::POS_READY)
?>


<h2><?= \Yii::t('shop', 'Images'); ?></h2>


<?php
echo Upload::widget([
    'model' => $image_form,
    'attribute' => 'images',
    'url' => ['add-image', 'id' => $productId, 'languageId' => $selectedLanguage->id],
    'sortable' => true,
    'maxNumberOfFiles' => 10,
    'acceptFileTypes' => new \yii\web\JsExpression('/(\.|\/)(gif|jpe?g|png)$/i'),
    'showPreviewFilename' => false
]);
?>


