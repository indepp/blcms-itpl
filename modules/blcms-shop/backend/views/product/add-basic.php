<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var array $prices
 * @var integer $selectedLanguageId
 * @var \common\models\shop\Product $product
 * @var \common\models\shop\ProductTranslation $productTranslation
 */
use common\components\user\models\UserGroup;
use common\models\shop\{
    PriceDiscountType, Product, ProductAvailability, ProductCountryTranslation, Vendor
};
use marqu3s\summernote\Summernote;
use yii\helpers\{
    ArrayHelper, Html, Url
};
use yii\widgets\ActiveForm;
use common\models\Tags;
use common\models\ShopFilterCategoryParams;
use kartik\select2\Select2;
use common\models\ShopFilterParams;
use yii\widgets\Pjax;
use common\models\shop\Currency;


$js = <<<JS
$('#product-tagsid').on('select2:select', function (e) {
    var params = e.params.data;
    if(params.disabled === false){return;}
    var select2Element = $(this);
    $.ajax({
        url: 'tags-create',
        method: 'post',
        data: params,
        dataType: 'json',
        async: false,
        success: function (data) {
            $('<option value="' + data.id + '">' + data.text + '</option>').appendTo(select2Element);
            var selection = select2Element.val();
            var index = selection.indexOf(data.text);
            if (index !== -1) {
                selection[index] = data.id.toString();
            }
            select2Element.val(selection).trigger('change');
        }
    });
});
// $(document).on('change', '.input-tree-ul input[type=radio]', function(e) {
//     $.pjax.reload({
//         "push":false,
//         "replace":false,
//         "timeout":1000,
//         "scrollTo":false,
//         "container":"#filter-param",
//         "data": {'category_id': $(this).val()}
//     });
// });
JS;
$this->registerJs($js, $this::POS_READY);

if(Yii::$app->request->get('category_id'))
    $product->category_id = Yii::$app->request->get('category_id');
?>

<?php $form = ActiveForm::begin([
    'method' => 'post',
    'enableClientValidation' => true,
    'action' => [
        'product/save',
        'id' => $product->id,
        'languageId' => $selectedLanguageId
    ]]);
?>
<!--SAVE BUTTON-->
<?= Html::submitInput(\Yii::t('shop', 'Save'), ['class' => 'btn btn-xs btn-primary m-r-xs pull-right']); ?>

<!--BASIC-->
<div id="basic">

    <!--MODERATION-->
    <?php if (Yii::$app->user->can('moderateProductCreation') && $product->status == Product::STATUS_ON_MODERATION) : ?>
        <h2><?= \Yii::t('shop', 'Moderation'); ?></h2>
        <p class="text-warning"><?= \Yii::t('shop', 'This product\'s status is "on moderation". You may accept or decline it.'); ?></p>
        <?= Html::a(\Yii::t('shop', 'Accept'), Url::toRoute(['change-product-status', 'id' => $product->id, 'status' => Product::STATUS_SUCCESS]), ['class' => 'btn btn-primary btn-xs']); ?>
        <?= Html::a(\Yii::t('shop', 'Decline'), Url::toRoute(['change-product-status', 'id' => $product->id, 'status' => Product::STATUS_DECLINED]), ['class' => 'btn btn-danger btn-xs']); ?>
        <hr>
    <?php endif; ?>

    <!--ERRORS-->
    <?php if ($product->hasErrors() || $productTranslation->hasErrors()): ?>
        <p class="text-danger">
            <?= \Yii::t('shop', 'Validation error. Please check all fields'); ?>
        </p>
    <?php endif; ?>

    <h2><?= \Yii::t('shop', 'Basic options'); ?></h2>

    <?php if (!\Yii::$app->user->can('createProductWithoutModeration') && $product->status == Product::STATUS_ON_MODERATION): ?>
        <p class="text-danger"><?= \Yii::t('shop', 'The product has not passed moderation yet'); ?></p>
    <?php endif; ?>

    <!--NAME-->
    <?= $form->field($productTranslation, 'title', [
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ])->label(\Yii::t('shop', 'Name'))
    ?>

    <!--PRODUCT TYPE-->
    <?= $form->field($productTranslation, 'product_type', [
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ])
    ?>

    <!--PRODUCT MODEL-->
    <?= $form->field($productTranslation, 'product_model', [
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ])
    ?>

    <div class="row">
        <div class="col-md-6">
            <div>
                <!--SKU-->
                <?php
                $options = [
                    'inputOptions' => [
                        'class' => 'form-control',
                    ]
                ];
                if($dis) {
                    $options['inputOptions']['disabled'] = 'disabled';
                }
                ?>
                <?= $field = $form->field($product, 'sku', $options)->label(\Yii::t('shop', 'SKU'))
                ?>
            </div>
            <div>
                <!--COUNTRY-->
                <?= $form->field($product, 'country_id', $options)->dropDownList(
                    ['' => '-- no countries --'] +
                    ArrayHelper::map(ProductCountryTranslation::find()->where(['language_id' => $selectedLanguageId])->all(), 'country_id', 'title')
                )->label(\Yii::t('shop', 'Country'));
                ?>
            </div>
            <div>
                <!--VENDOR-->
                <?= $form->field($product, 'vendor_id', $options)->dropDownList(
                    ['' => '-- no vendor --'] +
                    ArrayHelper::map(Vendor::find()->all(), 'id', 'title')
                )->label(\Yii::t('shop', 'Vendor'))
                ?>
            </div>
            <div>
                <!--AVAILABILITY-->
                <?= $form->field($product, 'availability', $options)->dropDownList(
                    ArrayHelper::map(ProductAvailability::find()->all(), 'id', 'translation.title')
                ); ?>
            </div>
            <div>
                <!--NUMBER-->
                <?= $form->field($product, 'number', $options)->textInput()
                ?>
            </div>
            <div>
                <!--POSITION-->
                <?= $form->field($product, 'position', $options)->textInput()
                ?>
            </div>
            <div>
                <!--TAGS-->
                <?= $form->field($product, 'tagsId')->widget(Select2::class, [
                    'data' => ArrayHelper::map(Tags::find()->all(), 'id', 'tagsTranslations.name'),
                    'options' => ['placeholder' => 'Select a Tag ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ])->label(Yii::t('tags', 'Tags'));
                ?>
            </div>
            <div>
                <?php Pjax::begin(['id' => 'filter-param']) ?>
                <!--NEW-FILTERS-->
                <?= $form->field($product, 'paramId')->widget(Select2::class, [
                    'data' => ArrayHelper::map($filterParams, 'param_id', 'param.shopFilterParamsTranslation.name'),
                    'options' => ['placeholder' => 'Select a Filter ...', 'multiple' => true],
                    'pluginOptions' => [
                        'tags' => true,
                        'tokenSeparators' => [',', ' '],
                        'maximumInputLength' => 10
                    ],
                ])->label(Yii::t('shop', 'Filters'));
                ?>
                <?php Pjax::end() ?>
            </div>
        </div>

        <div class="col-md-6">
            <?php if(!$dis): ?>
                <!-- MAIN CATEGORY -->
                <b><?= \Yii::t('shop', 'Main category'); ?></b>
                <?= \frontend\modules\shop\widgets\InputTree::widget([
                    'className' => \common\models\shop\Category::className(),
                    'form' => $form,
                    'model' => $product,
                    'attribute' => 'mainCategory',
                    'languageId' => $selectedLanguageId
                ]);
                ?>
            <?php endif; ?>

            <!-- RELATED CATEGORIES -->
            <?= $form->field($product, 'categoriesId')->widget(Select2::class, [
                'data' => ArrayHelper::map(\common\models\shop\Category::find()->all(), 'id', 'translation.title'),
                'options' => ['placeholder' => 'Select a Category ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10
                ],
            ])->label(Yii::t('tags', 'Related categories'));
            ?>

            <!-- CATEGORY FROM ROZETKA -->
            <b><?= \Yii::t('shop', 'Category from Rozetka'); ?></b>
            <?= \frontend\modules\shop\widgets\InputTree::widget([
                'className' => \common\models\shop\CategoryRozetka::className(),
                'form' => $form,
                'model' => $product,
                'attribute' => 'rozetka_category_id',
                'languageId' => $selectedLanguageId,
                'isOrderByPosition' => false,
                'isGetTranslation' => false
            ]);
            ?>
        </div>
    </div>

    <div class="">
        <!--NEW-->
        <div style="display: inline-block;">
            <?= $form->field($product, 'new', [
                'inputOptions' => [
                    'class' => '']
            ])->checkbox(); ?>
        </div>

        <!--SALE-->
        <div style="display: inline-block;">
            <?= $form->field($product, 'sale', [
                'inputOptions' => [
                    'class' => '']
            ])->checkbox(); ?>
        </div>

        <!--POPULAR-->
        <div style="display: inline-block;">
            <?= $form->field($product, 'popular', [
                'inputOptions' => [
                    'class' => '']
            ])->checkbox(); ?>
        </div>

        <!--SHOW-->
        <div style="display: inline-block;">
            <?php $product->show = ($product->isNewRecord) ? true : $product->show; ?>
            <?= $form->field($product, 'show', [
                'inputOptions' => [
                    'class' => '']
            ])->checkbox(); ?>
        </div>
    </div>

    <!--BASE PRICE-->
    <?php if (!empty($prices)): ?>
        <hr>
        <h2><?= \Yii::t('shop', 'Base price'); ?></h2>
        <?php foreach ($prices as $key => $price) : ?>
            <h3 class="text-center"><?= UserGroup::findOne($key)->translation->title; ?></h3>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($price, "[$key]price", $options)->textInput(['type' => 'number', 'step' => '0.01']); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($price, "[$key]currency")
                        ->dropDownList(
                            ['' => '--none--'] +
                            Currency::valueCurrency());
                    ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($price, "[$key]discount_type_id")
                        ->dropDownList(
                            ['' => '--none--'] +
                            ArrayHelper::map(PriceDiscountType::find()->asArray()->all(), 'id', 'title'));
                    ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($price, "[$key]discount")->textInput(['type' => 'number', 'step' => '0.01']); ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <hr>
    <h2><?= \Yii::t('shop', 'Product description'); ?></h2>
    <!--SHORT DESCRIPTION-->
    <?= $form->field($productTranslation, 'description', [
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ])->widget(Summernote::className())->label(\Yii::t('shop', 'Short description'));
    ?>

    <!--FULL TEXT-->
    <?= $form->field($productTranslation, 'full_text', [
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ])->widget(Summernote::className())->label(\Yii::t('shop', 'Full description'));
    ?>

    <!--SEO-->
    <hr>
    <h2><?= \Yii::t('shop', 'SEO options'); ?></h2>
    <div class="seo-url">
        <?= $form->field($productTranslation, 'seoUrl', [
            'inputOptions' => [
                'class' => 'form-control'
            ],
        ])->label('SEO URL')
        ?>
        <?= Html::button(\Yii::t('shop', 'Generate'), [
            'id' => 'generate-seo-url',
            'class' => 'btn btn-primary btn-generate',
            'url' => Url::to('generate-seo-url')
        ]); ?>
    </div>

    <?= $form->field($productTranslation, 'seoTitle', [
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ])->label(\Yii::t('shop', 'SEO title'))
    ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($productTranslation, 'seoDescription', [
                'inputOptions' => [
                    'class' => 'form-control'
                ]
            ])->textarea(['rows' => 3])->label(\Yii::t('shop', 'SEO description'))
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($productTranslation, 'seoKeywords', [
                'inputOptions' => [
                    'class' => 'form-control'
                ]
            ])->textarea(['rows' => 3])->label(\Yii::t('shop', 'SEO keywords'))
            ?>
        </div>
    </div>

    <hr>
    <h2><?= \Yii::t('shop', 'XML export'); ?></h2>
    <?php foreach ($availableMarketplaces as $placeKey => $placeLabel) { ?>
        <p><?= $form->field($product, 'selectedMarketplaces['. $placeKey .']')->checkbox(['label' => null])->label($placeLabel) ?></p>
    <?php } ?>

    <div class="ibox">
        <a href="<?= Url::to(['/shop/product']); ?>">
            <?= Html::button(\Yii::t('shop', 'Cancel'), [
                'class' => 'btn btn-xs btn-danger pull-right'
            ]); ?>
        </a>
        <input type="submit" class="btn btn-xs btn-primary m-r-xs pull-right" value="<?= \Yii::t('shop', 'Save'); ?>">
    </div>
</div>
<?php $form::end(); ?>

<?php if (!$product->isNewRecord): ?>
<div class="created-by">
    <p>
        <b>
            <?= \Yii::t('shop', 'Created by'); ?>:
        </b>
        <?= $product->ownerProfile->user->email ?? ''; // TODO: update this ?>
    </p>
    <p>
        <b>
            <?= \Yii::t('shop', 'Created at'); ?>:
        </b>
        <?= $product->creation_time; ?>

    </p>
</div>
<?php endif; ?>
