<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $this yii\web\View
 * @var $searchModel common\models\shop\SearchCategory
 * @var $dataProvider yii\data\ActiveDataProvider
 */

use bl\cms\itpl\shop\backend\assets\CategoriesIndexAsset;
use common\models\shop\Category;
use bl\multilang\entities\Language;
use yii\helpers\Html;

$this->title = Yii::t('shop', 'Marketplaces for XML upload');
$this->params['breadcrumbs'] = [
    Yii::t('shop', 'Shop'),
    $this->title
];
CategoriesIndexAsset::register($this);
?>

<div class="ibox">

    <div class="ibox-title">
        <h5>
            <i class="glyphicon glyphicon-list">
            </i>
            <?= Html::encode($this->title); ?>
        </h5>
    </div>

    <div class="ibox-content">
        <?php foreach ($availableMarcetplaces as $key => $value) { ?>
            <p><a href="/admin/shop/xml-upload/generate-to-<?= $key ?>"><?= $value ?></a></p>
        <?php } ?>

    </div>
</div>