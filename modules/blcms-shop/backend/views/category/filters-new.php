<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ShopFilterParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="row shop-category-filter">
    <div class="col-md-12">
        <div class="panel panel-default">
            <a href="<?= Url::to(['category/shop-filter-params-create', 'category_id' => $category->id]) ?>" class="btn btn-primary pull-right btn-top">
                <i class="fa fa-user-plus"></i> <?= Yii::t('articles', 'Add'); ?>
            </a>
            <div class="panel-heading">
                <i class="glyphicon glyphicon-list"></i>
                <?= Yii::t('shop', 'Filters') ?>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th class="col-md-8"><?= Yii::t('articles', 'Name'); ?></th>
                        <?php if (count($languages) > 1): ?>
                            <th class="col-lg-2"><?= Yii::t('articles', 'Language'); ?></th>
                        <?php endif; ?>
                        <th width="3%"><?= Yii::t('articles', 'Edit'); ?></th>
                        <th width="3%"><?= Yii::t('articles', 'Delete'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($models as $model): ?>
                        <tr>
                            <td>
                                <?= Html::a($model->param->shopFilterParamsTranslation->name ? $model->param->shopFilterParamsTranslation->name : Yii::t('shop', 'Add text filter'),
                                    ['category/shop-filter-params-update', 'id' => $model->param->id, 'category_id' => $category->id])
                                ?>
                            </td>

                            <td>
                                <?php if (count($languages) > 1): ?>
                                    <?php $translations = ArrayHelper::map($model->param->shopFilterParamsTranslations, 'language_id', 'name'); ?>
                                    <?php foreach ($languages as $language): ?>
                                        <a href="<?= Url::to(['category/shop-filter-params-update', 'id' => $model->param->id, 'category_id' => $category->id]) ?>"
                                           type="button"
                                           class="btn btn-<?= $translations[$language->id] ? 'primary' : 'danger' ?> btn-xs">
                                            <?= $language->name ?>
                                        </a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </td>

                            <td>
                                <a href="<?= Url::to(['category/shop-filter-params-update', 'id' => $model->param->id, 'category_id' => $category->id]) ?>"
                                   class="glyphicon glyphicon-edit text-warning btn btn-default btn-sm">
                                </a>
                            </td>

                            <td>
                                <a href="<?= Url::to(['category/shop-filter-params-delete', 'id' => $model->param->id]) ?>"
                                   class="glyphicon glyphicon-remove text-danger btn btn-default btn-sm">
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <a href="<?= Url::to(['category/shop-filter-params-create', 'category_id' => $category->id]) ?>" class="btn btn-primary pull-right">
                    <i class="fa fa-user-plus"></i> <?= Yii::t('articles', 'Add'); ?>
                </a>
            </div>
        </div>
    </div>
</div>