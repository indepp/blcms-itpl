<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @author Gutsulyak Vadim <guts.vadim@gmail.com>
 * @var $addParamModel common\models\shop\FilterParam
 * @var $addParamTranslationModel common\models\shop\FilterParam
 * @var $params common\models\shop\FilterParam[]
 */

?>


    <table class="table">
        <tr>
            <?php $form = ActiveForm::begin([
                'action' => 'add-filter-param',
                'method' => 'post',
//                'options' => ['data-pjax' => true]
            ]); ?>
                <?= $form->field($addParamModel, 'filter_id')->hiddenInput()->label(null) ?>
                <?= $form->field($addParamTranslationModel, 'language_id')->hiddenInput()->label(null) ?>

                <td><?= $form->field($addParamTranslationModel, 'title') ?></td>
                <td><?= $form->field($addParamTranslationModel, 'param_name') ?></td>
                <td><?= $form->field($addParamModel, 'key') ?></td>
                <td><?= $form->field($addParamModel, 'all_values')->checkbox() ?></td>
                <td colspan="2">
                    <?= Html::submitButton(Yii::t('backend.shop.filter.param', 'Add'), ['class' => 'btn btn-primary']) ?>
                </td>
            <?php ActiveForm::end(); ?>
        </tr>
        <!--<tr>
            <th><?/*= Yii::t('backend.shop.filter.param.translation', 'Title') */?></th>
            <th><?/*= Yii::t('backend.shop.filter.param.translation', 'Param Name') */?></th>
            <th><?/*= Yii::t('backend.shop.filter.param', 'All Values') */?></th>
            <th><?/*= Yii::t('backend.shop.filter.param', 'Delete') */?></th>
            <th><?/*= Yii::t('backend.shop.filter.param', 'Save') */?></th>
        </tr>-->
        <?php if(!empty($params)): ?>
            <?php foreach ($params as $param): ?>
                <tr>
                    <th><?= $param->translation->title ?></th>
                    <th><?= $param->translation->param_name ?></th>
                    <th><?= $param->key ?></th>
                    <th><?= $param->all_values ?></th>
                    <th>
                        <?php echo Html::a("X", ['delete-filter-param', 'paramId' => $param->id, 'categoryId' => $categoryId, 'languageId' => $languageId], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                            ],
                        ]) ?>
                    </th>
                    <th></th>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

