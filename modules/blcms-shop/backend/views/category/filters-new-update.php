<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $translations common\models\TagsTranslations[] */

?>
<div class="tags-update">

    <?= $this->render('_filters_form', [
        'translations' => $translations,
        'languages' => $languages,
        'param' => $param,
        'tags' => $tags,
        'relatedTags' => $relatedTags,
        'category_id' => $category_id
    ]) ?>

</div>
