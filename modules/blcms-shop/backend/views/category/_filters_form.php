<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use common\models\ShopFilterParams;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $param common\models\ShopFilterParams */
/* @var $form yii\widgets\ActiveForm */
/* @var $translations \common\models\ShopFilterParamsTranslation[] */

?>

<div class="filter-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($param, 'type_id')->dropDownList($param::getTypes())->label(Yii::t('articles', 'Filter-type')) ?>

    <?= $form->field($param, 'custom_url')->textInput()->hint('Здесь нужно вводить только машинное имя фильтра. Ссылка посроется автоматически') ?>

    <?= $form->field($param, 'redirect_url')->textInput()->hint('Здес нужно ввести ссылку на уже существующую страницу. Доменное имя сайта указывать не нужно. Ссылка должна начинаться из знака /') ?>

    <div class="row">
        <?= $form->field($param, 'tag_id')
            ->widget(\yii2mod\chosen\ChosenSelect::className(), [
                'items' => $tags
            ])->label(Yii::t('shop', 'Tag'));
        ?>

        <?php if (!empty($relatedTags)): ?>
            <?php foreach ($relatedTags as $relatedTag): ?>
                <tr>
                    <td>
                        <?= $relatedTag->tagsTranslations->name; ?>
                    </td>
                    <td class="text-center">
                        <?= Html::a(
                            '<span class="glyphicon glyphicon-remove"></span>',
                            \yii\helpers\Url::to(['/shop/category/shop-filter-params-update', 'id' => $param->id, 'category_id' => $category_id, 'remove_tag' => true]),
                            ['class' => 'btn btn-danger btn-xs']);
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <br>
        <?php endif; ?>
    </div>

    <?php foreach ($translations as $id => $translation) {  ?>
        <?= $form->field($translation, '[' . $id . ']name')->label($languages[$id]) ?>
        <?= $form->field($translation, '[' . $id . ']language_id')->hiddenInput(['value' => $id])->label(false) ?>
        <?= $form->field($translation, '[' . $id . ']redirect_url_by_language') ?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend-menu', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
