<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tags */

?>
<div class="tags-create">

    <?= $this->render('_filters_form', [
        'translations' => $translations,
        'languages' => $languages,
        'param' => $param,
        'tags' => $tags
    ]) ?>

</div>
