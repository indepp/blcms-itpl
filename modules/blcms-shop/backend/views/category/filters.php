<?php

use common\models\shop\FilterParam;
use common\models\shop\FilterParamTranslation;
use common\models\shop\ProductFilter;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $filter common\models\shop\ProductFilter */
/* @var $form ActiveForm */
/* @var $languageId integer */
?>
<div class="category-filter">
    <?php $form = ActiveForm::begin(['method' => 'post']); ?>

        <?= $form->field($filter, 'category_id')->hiddenInput()->label(null) ?>
        <?= $form->field($filter, 'show_price_filter')->checkbox() ?>
        <?= $form->field($filter, 'show_brand_filter')->checkbox() ?>
        <?= $form->field($filter, 'show_availability_filter')->checkbox() ?>
        <?= $form->field($filter, 'shop_params_filter')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('backend.shop.filter', 'Submit'), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

    <?php if(!$filter->isNewRecord && $filter->shop_params_filter): ?>
        <?php /*Pjax::begin([
            'id' => 'filter_params',
            'options' => [
                'skipOuterContainers' => true
            ],
            'enablePushState' => false,
        ]) */?>

            <?= $this->render('filter-params', [
                    'addParamModel' => new FilterParam([
                        'filter_id' => $filter->id,
                        'all_values' => true
                    ]),
                    'addParamTranslationModel' => new FilterParamTranslation([
                        'language_id' => $selectedLanguage->id
                    ]),
                    'params' => $filter->params,
                    'categoryId' => $category->id,
                    'languageId' => $selectedLanguage->id
            ]) ?>
<!--        --><?php //Pjax::end(); ?>
    <?php endif; ?>

</div><!-- category-filter -->
