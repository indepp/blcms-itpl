<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $product \common\models\shop\Product
 * @var $products \common\models\shop\Product[]
 * @var $relatedProducts \common\models\shop\RelatedProduct[]
 * @var $newRelatedProduct \common\models\shop\RelatedProduct
 * @var $selectedLanguage \bl\multilang\entities\Language
 * @var $languages \bl\multilang\entities\Language[]
 */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = \Yii::t('shop', 'Same products');
?>

<h1>
    <?= $this->title; ?>
</h1>

<div class="row">
    <div class="main col-md-8 col-md-offset-2">

        <?php $form = \yii\widgets\ActiveForm::begin(); ?>

        <div class="row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th class="col-md-10 text-center">
                        <?= \Yii::t('shop', 'Title'); ?>
                    </th>
                    <th class="col-md-2 text-center">
                        <?= \Yii::t('shop', 'Control'); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <?= $form->field($newSameProduct, 'same_product_id')
                            ->widget(\yii2mod\chosen\ChosenSelect::className(), [
                                'items' => ArrayHelper::map($products, 'id', 'translation.title'),
                            ]);
                        ?>
                    </td>
                    <td>
                        <?= Html::submitButton(\Yii::t('shop', 'Add'), ['class' => 'btn btn-primary']); ?>
                    </td>
                </tr>
                <?php if (!empty($sameProducts)): ?>
                    <?php foreach ($sameProducts as $sameProduct): ?>
                        <tr>
                            <td>
                                <?= $sameProduct->sameProduct->translation->title; ?>
                            </td>
                            <td class="text-center">
                                <?= Html::a(
                                    '<span class="glyphicon glyphicon-remove"></span>',
                                    Url::to(['/shop/same-product/remove', 'id' => $sameProduct->id]),
                                    ['class' => 'btn btn-danger btn-xs']);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
            </table>
        </div>

        <?php $form::end(); ?>
    </div>
</div>
