<?php
namespace bl\cms\itpl\shop\backend\assets;
use yii\web\AssetBundle;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */

class EditProductAsset extends AssetBundle
{
    public $sourcePath = '@vendor/indepp/blcms-itpl/modules/blcms-shop/backend/web';

    public $css = [
        'css/style.css',
    ];

    public $js = [
        'js/title-generation.js',
        'js/add-additional.js',
    ];

    public $depends = [
        'backend\modules\shop\assets\ProductAsset',
        'backend\modules\shop\assets\InputTreeAsset'
    ];
}