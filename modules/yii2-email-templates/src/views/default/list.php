<?php
/**
 * @link https://github.com/black-lamp/yii2-email-templates
 * @copyright Copyright (c) 2016 Vladimir Kuprienko
 * @license BSD 3-Clause License
 */

use bl\emailTemplates\EmailTemplates;
use bl\emailTemplates\models\entities\EmailTemplate;
use yii\helpers\Url;
use yii\helpers\Html;


/**
 * View for rendering list of email templates
 *
 * @var yii\web\View $this
 * @var EmailTemplate[] $templates
 * @var array $language
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */

$this->title = EmailTemplates::t('breadcrumbs', 'Email templates list');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ibox">

    <div class="ibox-title">
        <div class="ibox-tools">
            <h5>
                <i class="glyphicon glyphicon-list"></i>
                <?= $this->title ?>
            </h5>
            <?= Html::a(
                '<i class="fa fa-plus"></i> ' .
                EmailTemplates::t('backend', 'Create new template'),
                Url::toRoute([
                    'create',
                    'languageId' => key($language)
                ]),
                ['class' => 'btn btn-primary btn-xs pull-right']
            ) ?>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="ibox-content">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>
                    <?= EmailTemplates::t('backend', 'Key') ?>
                </th>
                <th>
                    <?= EmailTemplates::t('backend', 'Subject') ?>
                </th>
                <th>
                    <?= EmailTemplates::t('backend', 'Actions') ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($templates as $num => $template): ?>
                <tr>
                    <td>
                        <?= $num ?>
                    </td>
                    <td>
                        <?= $template->key ?>
                    </td>
                    <td>
                        <?= $template->translation->subject ?>
                    </td>
                    <td>
                        <?= Html::a(
                            EmailTemplates::t('backend', 'Edit'),
                            Url::toRoute([
                                'edit',
                                'templateId' => $template->id,
                                'languageId' => key($language)
                            ]),
                            ['class' => 'btn btn-xs btn-warning']
                        ) ?>
                        <?= Html::a(
                            EmailTemplates::t('backend', 'Delete'),
                            Url::toRoute(['delete', 'templateId' => $template->id]),
                            ['class' => 'btn btn-xs btn-danger']
                        ) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="ibox-footer">
        <?= Html::a(
            '<i class="fa fa-plus"></i> ' .
            EmailTemplates::t('backend', 'Create new template'),
            Url::toRoute([
                'create',
                'languageId' => key($language)
            ]),
            ['class' => 'btn btn-primary btn-xs pull-right']
        ) ?>
        <div class="clearfix"></div>
    </div>
</div>
