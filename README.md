#Itpl - Inspinia template

Views for modules:
- [blcms-Shop](https://github.com/black-lamp/blcms-shop)
- [blcms-Cart](https://github.com/black-lamp/blcms-cart)
- [blcms-Payment](https://github.com/black-lamp/blcms-payment)
- [blcms-StaticPage](https://github.com/black-lamp/blcms-staticpage)

### Installation

1) Add

```
{
    "type": "vcs",
    "url":  "https://github.com/GutsVadim/blcms-itpl"
}
```
to the ```repositories``` section of your `composer.json` file

and
   
```
"GutsVadim/blcms-itpl": "dev-master",
```
to the ```require``` section of your `composer.json` file

    
You may generate your Github token [here](https://github.com/settings/tokens).

2) Add `'bl\cms\itpl\InspiniaAsset'` to the `depends[]` in `backend/assets/AppAsset.php`

or
```php
\bl\cms\itpl\InspiniaAsset::register($this);
```
in your view file.

3) Add 
```
...
'view' => [
    'theme' => [
        'pathMap' => [
            ...
            '@vendor/black-lamp/blcms-shop/backend/views' => '@vendor/GutsVadim/blcms-itpl/modules/blcms-shop/backend/views',
            '@vendor/black-lamp/blcms-cart/backend/views' => '@vendor/GutsVadim/blcms-itpl/modules/blcms-cart/backend/views',
            '@vendor/black-lamp/blcms-staticpage/backend/views' => '@vendor/GutsVadim/blcms-itpl/modules/blcms-staticpage/backend/views',
            '@vendor/black-lamp/blcms-payment/backend/views' => '@vendor/GutsVadim/blcms-itpl/modules/blcms-payment/backend/views',
        ],
    ],
]
```

to the `components[]` in `backend/config/main.php`

4) To work links, that direct to the frontend, you must configure the backend config:
```
'components' => [
    ...
    'urlManagerFrontend' => [
        'class' => bl\multilang\MultiLangUrlManager::className(),
        'baseUrl' => '/',
        'showScriptName' => false,
        'enablePrettyUrl' => true,
        'enableDefaultLanguageUrlCode' => false,
        'rules' => [
            [
                'class' => bl\articles\UrlRule::className()
            ],
            [
                'class' => frontend\urlRules\shopRule::className(),
                'prefix' => 'shop'
            ],
        ]
    ]
]
```