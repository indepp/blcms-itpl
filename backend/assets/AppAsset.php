<?php
namespace bl\cms\itpl\backend\assets;

use yii\web\AssetBundle;

/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@vendor/indepp/blcms-itpl/backend/web/';

    public $css = [
        'css/layout.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\web\YiiAsset',
        'bl\cms\itpl\backend\assets\InspiniaAsset',
    ];
}