<?php
namespace bl\cms\itpl\backend\assets;

use yii\web\AssetBundle;

class InspiniaAsset extends AssetBundle
{
    public $sourcePath = '@vendor/indepp/blcms-itpl/backend/web/';

    public $css = [
        'inspinia/css/animate.css',
        'inspinia/css/style.css',
        'inspinia/font-awesome/css/font-awesome.css'
    ];
    public $js = [
        'inspinia/js/plugins/metisMenu/jquery.metisMenu.js',
        'inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js',
        'inspinia/js/inspinia.js',
        'inspinia/js/plugins/pace/pace.min.js'
    ];
    public $depends = [
    ];
}