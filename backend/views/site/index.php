<?php

/* @var $this yii\web\View */

use frontend\modules\cart\widgets\NewOrders;
use frontend\modules\shop\widgets\NewProducts;

$this->title = Yii::t('admin', 'Admin panel');


$date = new \DateTime('now');
$date->modify('-4 month');
$array = [];
for ($i = 1; $i < 7; $i++) {
    $ordersCost = \common\models\cart\Order::find()
        ->select('total_cost')
        ->where(['between', 'confirmation_time', $date->modify('-1 month')->format('Y-m') . '-01', $date->format('Y-m') . '-31'])
        ->sum('total_cost');

    $array[] = [$date->format('Y-m'), $ordersCost];
    $date->modify('+2 month');
}
$properties = ['sign' => \Yii::$app->formatter->currencyCode, 'position' => 'after'];
?>

<?php if (\Yii::$app->user->can('viewOrderList') || (\Yii::$app->user->can('viewOrderList'))) : ?>
    <div class="col-md-6">
        <?php if (\Yii::$app->user->can('viewOrderList')) : ?>
            <div class="ibox-content text-center p-md">
                <h2>
                    <?= Yii::t('cart', 'Dynamics of the amount of orders'); ?>
                </h2>
                <?= \frontend\modules\cart\widgets\Graph::widget([
                    'graphPoints' => [$array, $properties]
                ]); ?>
            </div>
        <?php endif; ?>

        <?php if (\Yii::$app->user->can('viewOrderList')) : ?>
            <div class="ibox-content text-center p-md">
                <h2><?= Yii::t('cart', 'New orders'); ?></h2>
                <?= NewOrders::widget(['num' => 10]); ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<div class="col-md-6">
    <div class="ibox-content text-center p-md">
        <h2><?= Yii::t('shop', 'New products'); ?></h2>
        <?= NewProducts::widget(['num' => 5]); ?>
    </div>
</div>



