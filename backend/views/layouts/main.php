<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $this \yii\web\View
 * @var $content string
 * @var Language $currLanguage
 */

use bl\cms\itpl\backend\assets\AppAsset;
use bl\multilang\entities\Language;
use bl\multilang\widgets\languageList\LanguageListWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $content string
 * @var Language $currLanguage
 */

$user = Yii::$app->user->identity;
$currLanguage = Language::getCurrent();

AppAsset::register($this);
backend\modules\shop\assets\AjaxLoaderAsset::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body <?= (!\Yii::$app->user->can('accessAdminPanel')) ? 'class="pool-image"' : ''; ?>>

    <?php $this->beginBody() ?>

    <div id="wrapper">

        <!--LEFT NAVIGATION BAR-->
        <?= $this->render('left-bar', ['user' => $user, 'currLanguage' => $currLanguage]); ?>

        <!--MAIN-->
        <div <?= (\Yii::$app->user->can('accessAdminPanel')) ? 'id="page-wrapper" class="gray-bg"' : '' ?>>
            <?php if (\Yii::$app->user->can('accessAdminPanel')): ?>

                <div class="row wrapper border-bottom white-bg page-heading header-panel">
                    <div class="col-lg-12 white-bg ">
                        <div class="col-md-10">
                            <h1>
                                <?= Html::encode($this->title) ?>
                            </h1>
                            <?= Breadcrumbs::widget([
                                'class' => 'breadcrumb',
                                'homeLink' => [
                                    'label' => Yii::t('yii', 'Home'),
                                    'url' => Url::toRoute(['/']),
                                    'itemprop' => 'url',
                                ],
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'activeItemTemplate' => '<li class=\"active\"><strong>{link}</strong></li>'
                            ]) ?>
                            <p class="on-site">
                                <a href="/" target="_blank">
                                    <span class="fa fa-arrow-circle-right "></span>
                                    <?= Yii::t('admin', 'On site'); ?>
                                </a>
                            </p>
                        </div>

                        <div class="col-md-2" style="top:50px;">
                            <?= LanguageListWidget::widget(); ?>
                        </div>
                    </div>
                </div>

                <div class="wrapper wrapper-content animated fadeInRight ecommerce">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row wrapper wrapper-content animated fadeInRight">
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endif; ?>

            <?php if (\Yii::$app->user->isGuest): ?>
                <div class="wrapper wrapper-content animated fadeInRight ecommerce">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row wrapper wrapper-content animated fadeInRight">
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>


    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>