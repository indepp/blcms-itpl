<?php
/**
 * @author Albert Gainutdinov <xalbert.einsteinx@gmail.com>
 *
 * @var $user common\components\user\models\User
 * @var $currLanguage bl\multilang\entities\Language
 */

use yii\bootstrap\Html;
use yii\helpers\Url;

?>

<?php if (\Yii::$app->user->can('accessAdminPanel')) : ?>
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="text-center dropdown profile-element">

                        <!--AVATAR-->
                        <?php if (!empty($user->profile->avatar)) : ?>
                            <?= Html::img('/images/avatars/' . $user->profile->avatar . '-small.jpg',
                                [
                                    'alt' => 'image',
                                    'class' => 'img-circle'
                                ]); ?>
                        <?php endif; ?>

                        <!--USER NAME-->
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold">
                                <?php
                                if (!empty($user->profile)) {
                                    if (!empty($user->profile->name) && !empty($user->profile->surname)) {
                                        $name = $user->profile->name . ' ' . $user->profile->surname;
                                    } else $name = $user->email;
                                } else $name = $user->email;
                                echo $name;
                                ?>

                                <b class="caret"></b>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="<?= Url::toRoute(['/user/settings/']) ?>">
                                    <i class="fa fa-user"></i>
                                    <span>
                                                <?= \Yii::t('backend-menu', 'My account'); ?>
                                            </span>
                                </a>
                            </li>
                            <li>
                                <?= Html::a(
                                    Html::tag('i', '', ['class' => 'fa fa-sign-out'])
                                    . \Yii::t('backend-menu', 'Logout'),
                                    ['//user/security/logout'],
                                    ['class' => 'btn btn-link', 'data-method' => 'post']); ?>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="<?= Url::toRoute(['/']) ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>
                            <?= \Yii::t('backend-menu', 'Control Panel'); ?>
                        </span>
                    </a>
                </li>
                <?php if (\Yii::$app->user->can('editStaticPage')) : ?>
                    <li>
                        <a href="<?= Url::toRoute(['/seo/static/save-page',
                            'page_key' => 'main', 'languageId' => $currLanguage->id
                        ]) ?>">
                            <i class="fa fa-home"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Main page'); ?>
                                </span>
                        </a>
                    </li>
                <?php endif; ?>

                <!--CATALOG-->
                <?php if (\Yii::$app->user->can('viewShopCategoryList') ||
                    \Yii::$app->user->can('viewProductList') ||
                    \Yii::$app->user->can('viewShopCategoryList') ||
                    \Yii::$app->user->can('viewAttributeList') ||
                    \Yii::$app->user->can('viewCountryList') ||
                    \Yii::$app->user->can('viewCurrencyList') ||
                    \Yii::$app->user->can('viewFilterList') ||
                    \Yii::$app->user->can('viewProductAvailabilityList') ||
                    \Yii::$app->user->can('viewVendorList') ||
                    \Yii::$app->user->can('createProduct')
                ) : ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-book"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Catalog'); ?>
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <?php if (\Yii::$app->user->can('editStaticPage')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/seo/static/save-page',
                                        'page_key' => 'shop', 'languageId' => $currLanguage->id
                                    ]) ?>">
                                        <i class="fa fa-home"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Shop main page'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewShopCategoryList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/category']) ?>">
                                        <i class="fa fa-tags"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Categories'); ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/rozetka-category']) ?>">
                                        <i class="fa fa-tags"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Rozetka categories'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewProductList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/product']) ?>">
                                        <i class="fa fa-list-ul"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Products'); ?>
                                        </span>
                                    </a>
                                </li>
                                <?php if (\Yii::$app->user->can('viewShopCategoryList')) : ?>
                                    <li>
                                        <a href="<?= Url::toRoute(['/shop/product/additional-product']) ?>">
                                            <i class="fa fa-list-ul"></i>
                                            <span>
                                            <?= \Yii::t('backend-menu', 'Additional products'); ?>
                                        </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/comment']) ?>">
                                        <i class="fa fa-list-ul"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Comments'); ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/xml']) ?>">
                                        <i class="fa fa-list-ul"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'XML export'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewCountryList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/country']) ?>">
                                        <i class="fa fa-flag"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Countries'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewVendorList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/vendor']) ?>">
                                        <i class="fa fa-shield"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Vendors'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewAttributeList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/attribute']) ?>">
                                        <i class="fa fa-th-list"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Attributes'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewProductAvailabilityList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/product-availability']) ?>">
                                        <i class="fa fa-th-list"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', Yii::t('shop', 'Availability statuses')); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewFilterList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/filter']) ?>">
                                        <i class="fa fa-filter"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Filters'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewCurrencyList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/currency']) ?>">
                                        <i class="fa fa-money"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Currency'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('createProduct')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/shop/product/save', 'languageId' => $currLanguage]) ?>">
                                        <i class="fa fa-plus-circle"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Add product'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>

                <!--ORDERS-->
                <?php if (\Yii::$app->user->can('editStaticPage') ||
                    \Yii::$app->user->can('viewOrderList') ||
                    \Yii::$app->user->can('viewOrderStatusList') ||
                    \Yii::$app->user->can('viewDeliveryMethodList') ||
                    \Yii::$app->user->can('viewPaymentMethodList')
                ) : ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-shopping-cart"></i>
                            <span>
                                <?= \Yii::t('backend-menu', 'Orders'); ?>
                            </span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="nav nav-second-level">
                            <?php if (\Yii::$app->user->can('editStaticPage')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/seo/static/save-page',
                                        'page_key' => 'cart', 'languageId' => $currLanguage->id
                                    ]) ?>">
                                        <i class="fa fa-home"></i>
                                        <span>
                                        <?= Yii::t('backend-menu', 'Cart page'); ?>
                                    </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewOrderList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/cart/order']) ?>">
                                        <i class="fa fa-list-ul"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Order list'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewOrderStatusList')): ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/cart/order-status/']) ?>">
                                        <i class="fa fa-flag"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Order statuses'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewDeliveryMethodList')): ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/cart/delivery-method']) ?>">
                                        <i class="fa fa-truck"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Delivery methods'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewPaymentMethodList')): ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/payment/']) ?>">
                                        <i class="fa fa-money"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Payment methods'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <?= \frontend\modules\cart\widgets\OrderCounter::widget(); ?>
                    </li>
                <?php endif; ?>

                <?php if (\Yii::$app->user->can('moderatePartnerRequest')) : ?>
                    <li>
                        <a href="<?= Url::toRoute(['/shop/partners']) ?>">
                            <i class="fa fa-users"></i>
                            <span>
                                                <?= \Yii::t('backend-menu', 'Partners'); ?>
                                            </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php if (\Yii::$app->user->can('viewOrderList')) : ?>
                    <!--CLIENTS-->
                    <li>
                        <a href="<?= Url::toRoute(['/newsletter']) ?>">
                            <i class="fa fa-child"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Clients'); ?>
                                </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php if (\Yii::$app->user->can('viewAlbumList') ||
                    \Yii::$app->user->can('viewImageList')
                ) : ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-picture-o"></i>
                            <span>Галерея</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <?php if (\Yii::$app->user->can('editStaticPage')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/seo/static/save-page',
                                        'page_key' => 'gallery', 'languageId' => $currLanguage->id
                                    ]) ?>">
                                        <i class="fa fa-home"></i>
                                        <span>
                                        <?= Yii::t('backend-menu', 'Main page'); ?>
                                    </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewAlbumList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/gallery/album']) ?>">
                                        <i class="fa fa-folder-o"></i>
                                        <span>
                                        <?= Yii::t('backend-menu', 'Albums'); ?>
                                    </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewImageList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/gallery/image']) ?>">
                                        <i class="fa fa-picture-o"></i>
                                        <span>
                                        <?= Yii::t('backend-menu', 'Photos'); ?>
                                    </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('createImage')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/gallery/image/create']) ?>">
                                        <i class="fa fa-plus-circle"></i>
                                        <span>
                                        <?= Yii::t('backend-menu', 'Add new'); ?>
                                    </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>


                <!-- SLIDER -->
                <li>
                    <a href="#">
                        <i class="fa fa-th"></i>
                        <span><?= Yii::t('backend-menu', 'Sliders'); ?></span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?= Url::toRoute(['/slider']) ?>">
                                <i class="fa fa-picture-o"></i>
                                <span>
                                    <?= Yii::t('backend-menu', 'Sliders'); ?>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::toRoute(['/slider-tags']) ?>">
                                <i class="fa fa-tags"></i>
                                <span>
                                    <?= Yii::t('backend-menu', 'Tags'); ?>
                                </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- SLIDER -->

                <?php if (\Yii::$app->user->can('viewArticleList') ||
                    \Yii::$app->user->can('viewArticleCategoryList')
                ): ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-file"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Articles'); ?>
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <?php if (\Yii::$app->user->can('viewArticleList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/articles/article']) ?>">
                                        <i class="fa fa-file-text"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Articles list'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (\Yii::$app->user->can('viewArticleCategoryList')) : ?>
                                <li>
                                    <a href="<?= Url::toRoute(['/articles/category']) ?>">
                                        <i class="fa fa-list-ul"></i>
                                        <span>
                                            <?= \Yii::t('backend-menu', 'Categories'); ?>
                                        </span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('viewArticleCategoryList')) : ?>
                    <li>
                        <a href="<?= Url::toRoute(['/articles/tags']) ?>">
                            <i class="fa fa-tags"></i>
                            <span>
                                <?= \Yii::t('backend-menu', 'Tags'); ?>
                            </span>
                        </a>
                    </li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('viewStaticPages')) : ?>
                    <li>
                        <a href="<?= Url::toRoute(['/seo/static/']) ?>">
                            <i class="fa fa-file-text-o"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Static pages'); ?>
                                </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php if (\Yii::$app->user->can('manageLegalAgreement')) : ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-book"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Legal information'); ?>
                                </span>
                            <span class="fa arrow"></span>
                        </a>

                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= Url::toRoute(['/legal/default/list']) ?>">
                                    <i class="fa fa-home"></i>
                                    <span>
                                <?= \Yii::t('backend-menu', 'Legal information list'); ?>
                            </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::toRoute(['/legal-seo']) ?>">
                                    <i class="fa fa-home"></i>
                                    <span>
                                <?= \Yii::t('backend-menu', 'SEO legal information list'); ?>
                            </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>

                <?php if (\Yii::$app->user->can('viewPartnerRequest') ||
                    (\Yii::$app->user->can('viewOrderList'))
                ) : ?>
                    <!--EMAIL TEMPLATES-->
                    <li>
                        <a href="<?= Url::toRoute(['/email-templates']) ?>">
                            <i class="fa fa-envelope"></i>
                            <span>
                            <?= \Yii::t('backend-menu', 'Email templates') ?>
                        </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php if (\Yii::$app->user->can('viewPartnerRequest') ||
                    (\Yii::$app->user->can('viewOrderList')) ||
                    (\Yii::$app->user->can('createProductWithoutModeration'))
                ) : ?>
                    <li>
                        <a href="<?= Url::toRoute(['/translation
                    ']) ?>">
                            <i class="fa fa-language"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Translations'); ?>
                                </span>
                        </a>
                    </li>
                <?php endif; ?>

                <!--USERS-->
                <?php if (\Yii::$app->user->can('createUser') || \Yii::$app->user->can('createPermission')) : ?>
                    <li>
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>
                                    <?= \Yii::t('backend-menu', 'Users'); ?>
                                </span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?= Url::toRoute(['/user/admin/']) ?>">
                                    <i class="fa fa-user"></i>
                                    <span>
                                        <?= \Yii::t('backend-menu', 'User management'); ?>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::toRoute(['/user/admin/show-user-groups']) ?>">
                                    <i class="fa fa-group"></i>
                                    <span>
                                        <?= \Yii::t('backend-menu', 'User groups'); ?>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= Url::toRoute(['/rbac']) ?>">
                                    <i class="fa fa-lock"></i>
                                    <span>
                                            <?= \Yii::t('backend-menu', 'User permissions'); ?>
                                        </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>

                <li>
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span>
                                    <?= \Yii::t('backend-menu', 'Settings'); ?>
                                </span>
                        <span class="fa arrow"></span>

                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?= Url::toRoute(['/user/settings/']) ?>">
                                <i class="fa fa-user"></i>
                                <span>
                                        <?= \Yii::t('backend-menu', 'My account'); ?>
                                    </span>
                            </a>
                        </li>
                        <?php if (\Yii::$app->user->can('viewLanguages')) : ?>
                            <li>
                                <a href="<?= Url::toRoute(['/languages/']) ?>">
                                    <i class="fa fa-language"></i>
                                    <span>
                                        <?= \Yii::t('backend-menu', 'Languages'); ?>
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if (\Yii::$app->user->can('changeAppConfiguration')) : ?>
                            <li>
                                <a href="<?= Url::toRoute(['/redirect/']) ?>">
                                    <i class="fa fa-exchange"></i>
                                    <span>
                                        <?= \Yii::t('backend-menu', 'Redirects'); ?>
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
<?php endif; ?>
